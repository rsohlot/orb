/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function terminal_password(term, parameters) {
	if (parameters.length == 0) {
		$.post('/terminal/password', {
			path: term.path,
		}).done(function(data) {
			term.write('Password set for users:');
			$(data).find('username').each(function() {
				term.write(' ' + $(this).text());
			});
			term.writeln('');
			terminal_done(term);
		}).fail(function(result) {
			term.writeln('No password settings found.');
			terminal_done(term);
		});
		return;
	}

	username = parameters[0];

	if (username != 'remove') {
		term.write('Password: ');
		terminal_input(function(password) {
			if (password == undefined) {
				terminal_done(term);
				return;
			}

			$.post('/terminal/password', {
				path: term.path,
				username: username,
				password: password
			}).done(function(data) {
				terminal_done(term);
			}).fail(function() {
				terminal_done(term);
			});
		}, false);
	} else {
		$.post('/terminal/password', {
			path: term.path,
			username: 'remove',
		}).done(function(data) {
			terminal_done(term);
		}).fail(function() {
			terminal_done(term);
		});
	}
}
