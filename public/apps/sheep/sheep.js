/* Orb Sheep application
 *
 * Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function sheep_open(filename = undefined) {
	orb_load_javascript('/apps/sheep/esheep.js');

	var sheep = new eSheep();
	sheep.Start();
}

$(document).ready(function() {
	orb_startmenu_add('Sheep', '/apps/sheep/sheep.png', sheep_open);
});
