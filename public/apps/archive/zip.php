<?php
	namespace Orb;

	if (defined("ORB_VERSION") == false) exit;

	class zip extends archiver_library implements archiver_interface {
		private $archive = null;
		private $zip = null;

		public function open($archive) {
			$zip = new \ZipArchive;
			if ($zip->open($archive) !== true) {
				return false;
			}

			$this->archive = $archive;
			$this->zip = $zip;

			return true;
		}

		public function list() {
			if ($this->zip === null) {
				return false;
			}

			$list = array();
			for ($i = 0; $i < $this->zip->numFiles; $i++) {
				$stat = $this->zip->statIndex($i);
				$this->list_add($list, $stat["name"], $stat["size"]);
			}

			return $list;
		}

		public function extract($filename, $directory) {
			if ($this->zip === null) {
				return false;
			}

			try {
				$this->zip->extractTo($directory, $filename);
			} catch (Exception $e) {
				return false;
			}

			return $directory."/".$filename;
		}
	}
?>
